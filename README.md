太长不读
=====

## 概述

太长不读「TLDR: Too Long, Don't Read」，是一个微信小程序简化版linux man手册。它可以查询并列出linux命令的常用场景和示例，简单易懂。

## 预览
### 界面截图预览

<div align=center>
  <img src='https://gitee.com/konglinglong/tldr/raw/master/resources/小程序界面拼接v2_1.jpg' alt='preview' />
</div>

<div align=center>
  <img src='https://gitee.com/konglinglong/tldr/raw/master/resources/小程序界面拼接v2_2.jpg' alt='preview' />
</div>

### 手机扫码预览
<div align=center>
  <img src='https://gitee.com/konglinglong/tldr/raw/master/resources/小程序二维码_430.jpg' alt='preview' />
</div>

## 使用
#### 1. 从GitHub下载源码
#### 2. 使用微信开发者工具直接导入project目录

## 赞赏
<div align=center>
  <img src='https://gitee.com/konglinglong/tldr/raw/master/resources/zsm.png' alt='preview' />
</div>

## License
The MIT License(http://opensource.org/licenses/MIT)

请自由地享受和参与开源


## 贡献

如果你有好的意见或建议，欢迎给我们提issue或pull request。

